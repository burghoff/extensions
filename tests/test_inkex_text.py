# coding=utf-8
import inkex
from inkex.tester import ComparisonMixin, TestCase

import os
import sys
import subprocess

# How to generate a test output:
# [python binary] [test_inkex_text.py] modify_svg [test_input.svg]


def modify_svg(input_file):
    mydir = os.path.dirname(os.path.abspath(__file__))
    if os.path.isdir(os.path.join(mydir, "packages")):  # Local testing
        sys.path.append(os.path.join(mydir, "packages"))
        sys.path.append(os.path.join(mydir, "packages", "python_fontconfig"))
    import inkex.text
    from inkex import load_svg

    svg = load_svg(input_file).getroot()

    # For testing purposes, we should only use fonts installed on the pipeline
    # Replace any other fonts with DejaVu Sans
    # (In order to pass DejaVu Sans should be installed)
    pipelinefonts = PIPELINE_DATA["pangofaces"].values()
    from inkex.text.font_properties import font_style

    for el in svg.iter():
        if (
            isinstance(
                el, (inkex.TextElement, inkex.Tspan, inkex.FlowRoot, inkex.FlowPara)
            )
            and font_style(el.ccascaded_style) not in pipelinefonts
        ):
            sty = inkex.Style(list(el.cstyle.items()))
            if "-inkscape-font-specification" in sty:
                del sty["-inkscape-font-specification"]
            sty.update(PIPELINE_DATA["pangofaces"]["DejaVu Sans"])
            el.cstyle = sty
            el.parsed_text = None

    # Make text highlights
    for el in svg.iter():
        if isinstance(el, (inkex.TextElement, inkex.FlowRoot)):
            el.parsed_text.make_highlights("char")

    # Write to disk, removing any existing file
    newfile = input_file + "_modfortest"
    if os.path.isfile(newfile):
        os.remove(newfile)

    inkex.command.write_svg(svg, newfile)

    # Save information about Pango so it can be printed if needed
    pr = inkex.text.font_properties.PangoRenderer()
    test = dict(zip(pr.face_strings, (pr.face_css)))
    data = {
        "haspango": inkex.text.font_properties.HASPANGO,
        "haspangoFT2": inkex.text.font_properties.HASPANGOFT2,
        "pangofaces": test,
    }
    import pickle

    outfile = input_file + "_outputdata"
    with open(outfile, "wb") as file:
        pickle.dump(data, file)


class HighlightText(inkex.EffectExtension):
    def add_arguments(self, pars):
        pars.add_argument("--testtype")

    def effect(self):
        # If the inkex.gui tester has been loaded already, the correct typelibs
        # cannot be found in gi because it has already been imported.
        # Reloading the import will not help.
        # Instead, we run in a new subprocess so we can get a fresh gi import.
        script_name = os.path.abspath(__file__)
        process = subprocess.Popen(
            [sys.executable, script_name, "modify_svg", self.options.input_file],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            text=True,
        )
        process.wait()
        stdout, stderr = process.communicate()
        if len(stderr) > 0:
            print(stderr)

        import pickle

        outfile = self.options.input_file + "_outputdata"
        if os.path.isfile(outfile):
            with open(outfile, "rb") as file:
                dataout = pickle.load(file)
            os.remove(outfile)

        # Only needed to figure out what fonts are on the pipeline
        # print(dataout)

        newfile = self.options.input_file + "_modfortest"
        if os.path.isfile(newfile):
            self.document = inkex.load_svg(newfile)
            os.remove(newfile)


from inkex.tester.filters import CompareWithoutIds
from inkex.tester.filters import Compare
import re


# Turn numbers into shorter standard formats
class CompareNumericFuzzy2(Compare):
    @staticmethod
    def filter(contents):
        prec_else = 0
        prec_trfm = 3

        # Standardize transforms to matrix()
        spans = []
        repls = []
        tfm_pattern = rb'\btransform\s*=\s*(["\'])(.+?)\1'
        tfms = list(re.finditer(tfm_pattern, contents))
        for m in tfms:
            spans.append(m.span())
            tcnts = m.group(1)  # contents of transform tag
            newstr = matrix_to_transform(
                transform_to_matrix(tcnts),
                precision_abcd=prec_trfm,
                precision_ef=prec_else,
            )
            repls.append(b'transform="' + newstr + b'"')
        contents2 = Replace_Spans(contents, spans, repls)

        # Round other numbers to 0 digits, ignoring numbers in transforms
        nums = list(re.finditer(rb"-?\d+\.\d+(e[+-]\d+)?", contents2))
        tfms = list(re.finditer(tfm_pattern, contents2))
        tfms = [mt.span() for mt in tfms]
        spans = []
        repls = []
        ti = 0
        # current transform we are looking at
        for m in nums:
            s, e = m.span()
            while ti < len(tfms) and tfms[ti][1] <= e:
                ti += 1
            intfm = (
                ti < len(tfms) and s > tfms[ti][0] and e < tfms[ti][1]
            )  # in next tfm
            # print((m.group(0),intfm))
            if not (intfm):
                fmt = b"%." + f"{prec_else}".encode("utf-8") + b"f"
                repl = fmt % (float(m.group(0)) + 0)  # Adding 0 changes -0 to 0
                spans.append((s, e))
                # if contents2[s-3:s] in (b'x="',b'y="'):
                #     print((contents2[s-3:e+1],repl))
                repls.append(repl)
        contents3 = Replace_Spans(contents2, spans, repls)
        contents = contents3

        # func = lambda m: b"%.1f" % (float(m.group(0)))
        # contents = re.sub(rb"\d+\.\d+(e[+-]\d+)?", func, contents)
        contents = re.sub(rb"(\d\.\d+?)0+\b", rb"\1", contents)
        contents = re.sub(rb"(\d)\.0+(?=\D|\b)", rb"\1", contents)

        # Remove empty dx="0" or dy="0" values and similar
        contents = re.sub(rb'\b(dx|dy)\s*=\s*(["\'])(-?0(\.0)?)\2', rb"", contents)

        return contents


# Converts a transform string into a standard matrix
def transform_to_matrix(transform):
    # Regular expression pattern to match transform functions
    pattern = rb"\b(scale|translate|rotate|skewX|skewY|matrix)\(([^\)]*)\)"
    # Initialize result matrix

    null = [[1, 0, 0], [0, 1, 0], [0, 0, 1]]
    matrix = [[1, 0, 0], [0, 1, 0], [0, 0, 1]]
    if b"none" in transform or not re.search(pattern, transform):
        return null
    # Find all transform functions
    for match in re.finditer(pattern, transform):
        transform_type = match.group(1)
        transform_args = list(map(float, re.split(rb"[\s,]+", match.group(2))))

        if transform_type == b"scale":
            # Scale transform
            if len(transform_args) == 1:
                sx = sy = transform_args[0]
            elif len(transform_args) == 2:
                sx, sy = transform_args
            else:
                return null
            matrix = matrix_multiply(matrix, [[sx, 0, 0], [0, sy, 0], [0, 0, 1]])
        elif transform_type == b"translate":
            # Translation transform
            if len(transform_args) == 1:
                tx = transform_args[0]
                ty = 0
            elif len(transform_args) == 2:
                tx, ty = transform_args
            else:
                return null
            matrix = matrix_multiply(matrix, [[1, 0, tx], [0, 1, ty], [0, 0, 1]])
        elif transform_type == b"rotate":
            # Rotation transform
            if len(transform_args) == 1:
                angle = transform_args[0]
                cx = cy = 0
            elif len(transform_args) == 3:
                angle, cx, cy = transform_args
            else:
                return null
            angle = angle * math.pi / 180  # Convert angle to radians
            matrix = matrix_multiply(matrix, [[1, 0, cx], [0, 1, cy], [0, 0, 1]])
            matrix = matrix_multiply(
                matrix,
                [
                    [math.cos(angle), -math.sin(angle), 0],
                    [math.sin(angle), math.cos(angle), 0],
                    [0, 0, 1],
                ],
            )
            matrix = matrix_multiply(matrix, [[1, 0, -cx], [0, 1, -cy], [0, 0, 1]])
        elif transform_type == b"skewX":
            # SkewX transform
            if len(transform_args) == 1:
                angle = transform_args[0]
            else:
                return null
            angle = angle * math.pi / 180  # Convert angle to radians
            matrix = matrix_multiply(
                matrix, [[1, math.tan(angle), 0], [0, 1, 0], [0, 0, 1]]
            )
        elif transform_type == b"skewY":
            # SkewY transform
            if len(transform_args) == 1:
                angle = transform_args[0]
            else:
                return null
            angle = angle * math.pi / 180  # Convert angle to radians
            matrix = matrix_multiply(
                matrix, [[1, 0, 0], [math.tan(angle), 1, 0], [0, 0, 1]]
            )
        elif transform_type == b"matrix":
            # Matrix transform
            if len(transform_args) == 6:
                a, b, c, d, e, f = transform_args
            else:
                return null
            matrix = matrix_multiply(matrix, [[a, c, e], [b, d, f], [0, 0, 1]])
    # Return the final matrix
    return matrix


def matrix_to_transform(matrix, precision_abcd=3, precision_ef=0):
    # Extract matrix elements
    a, c, e = matrix[0]
    b, d, f = matrix[1]

    # Round a, b, c, d elements to specified precision
    # Adding 0 changes -0 to 0
    a = round(a, precision_abcd) + 0
    b = round(b, precision_abcd) + 0
    c = round(c, precision_abcd) + 0
    d = round(d, precision_abcd) + 0

    # Round e, f elements to specified precision
    e = round(e, precision_ef) + 0
    f = round(f, precision_ef) + 0

    # Construct transform string
    transform = f"matrix({a} {b} {c} {d} {e} {f})"
    return transform.encode("utf-8")


import math


def matrix_multiply(a, b):
    # Initialize result matrix
    result = [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
    # Multiply the matrices
    for i in range(3):
        for j in range(3):
            for k in range(3):
                result[i][j] += a[i][k] * b[k][j]
    return result


# Replace a list of spans with a list of replacement strings
def Replace_Spans(string, spans, repls):
    if len(spans) == 0:
        return string
    # Sort in order
    zipped = zip(spans, repls)
    sorted_zipped = sorted(zipped, key=lambda x: x[0][0])
    spans, repls = zip(*sorted_zipped)

    repls = iter(repls)
    result = []
    last_end = 0
    for start, end in spans:
        result.append(string[last_end:start])
        result.append(next(repls))
        last_end = end
    result.append(string[last_end:])
    return rb"".join(result)


class TextRegularText(ComparisonMixin, TestCase):
    effect_class = HighlightText
    compare_file = "svg/text_regular_types.svg"
    comparisons = [("--testtype=text",)]
    compare_filters = [CompareNumericFuzzy2()]


class TextFlowText(ComparisonMixin, TestCase):
    effect_class = HighlightText
    compare_file = "svg/text_flow_types.svg"
    comparisons = [("--testtype=flow",)]
    compare_filters = [CompareNumericFuzzy2()]


# List of the fonts installed on the pipeline tester

if __name__ == "__main__":
    from inkex import Style

    # Fonts on the tester pipeline
    PIPELINE_DATA = {
        "haspango": True,
        "haspangoFT2": True,
        "pangofaces": {
            "DejaVu Sans": Style(
                [
                    ("font-family", "DejaVu Sans"),
                    ("font-stretch", "normal"),
                    ("font-weight", "400"),
                    ("font-style", "normal"),
                ]
            ),
            "DejaVu Sans Italic": Style(
                [
                    ("font-family", "DejaVu Sans"),
                    ("font-stretch", "normal"),
                    ("font-weight", "400"),
                    ("font-style", "italic"),
                ]
            ),
            "DejaVu Sans Bold Italic": Style(
                [
                    ("font-family", "DejaVu Sans"),
                    ("font-stretch", "normal"),
                    ("font-weight", "700"),
                    ("font-style", "italic"),
                ]
            ),
            "DejaVu Sans Bold": Style(
                [
                    ("font-family", "DejaVu Sans"),
                    ("font-stretch", "normal"),
                    ("font-weight", "700"),
                    ("font-style", "normal"),
                ]
            ),
            "DejaVu Sans Mono": Style(
                [
                    ("font-family", "DejaVu Sans Mono"),
                    ("font-stretch", "normal"),
                    ("font-weight", "400"),
                    ("font-style", "normal"),
                ]
            ),
            "DejaVu Sans Mono Italic": Style(
                [
                    ("font-family", "DejaVu Sans Mono"),
                    ("font-stretch", "normal"),
                    ("font-weight", "400"),
                    ("font-style", "italic"),
                ]
            ),
            "DejaVu Sans Mono Bold Italic": Style(
                [
                    ("font-family", "DejaVu Sans Mono"),
                    ("font-stretch", "normal"),
                    ("font-weight", "700"),
                    ("font-style", "italic"),
                ]
            ),
            "DejaVu Sans Mono Bold": Style(
                [
                    ("font-family", "DejaVu Sans Mono"),
                    ("font-stretch", "normal"),
                    ("font-weight", "700"),
                    ("font-style", "normal"),
                ]
            ),
            "DejaVu Serif": Style(
                [
                    ("font-family", "DejaVu Serif"),
                    ("font-stretch", "normal"),
                    ("font-weight", "400"),
                    ("font-style", "normal"),
                ]
            ),
            "DejaVu Serif Italic": Style(
                [
                    ("font-family", "DejaVu Serif"),
                    ("font-stretch", "normal"),
                    ("font-weight", "400"),
                    ("font-style", "italic"),
                ]
            ),
            "DejaVu Serif Bold Italic": Style(
                [
                    ("font-family", "DejaVu Serif"),
                    ("font-stretch", "normal"),
                    ("font-weight", "700"),
                    ("font-style", "italic"),
                ]
            ),
            "DejaVu Serif Bold": Style(
                [
                    ("font-family", "DejaVu Serif"),
                    ("font-stretch", "normal"),
                    ("font-weight", "700"),
                    ("font-style", "normal"),
                ]
            ),
            "Monospace": Style(
                [
                    ("font-family", "Monospace"),
                    ("font-stretch", "normal"),
                    ("font-weight", "400"),
                    ("font-style", "normal"),
                ]
            ),
            "Monospace Bold": Style(
                [
                    ("font-family", "Monospace"),
                    ("font-stretch", "normal"),
                    ("font-weight", "700"),
                    ("font-style", "normal"),
                ]
            ),
            "Monospace Italic": Style(
                [
                    ("font-family", "Monospace"),
                    ("font-stretch", "normal"),
                    ("font-weight", "400"),
                    ("font-style", "italic"),
                ]
            ),
            "Monospace Bold Italic": Style(
                [
                    ("font-family", "Monospace"),
                    ("font-stretch", "normal"),
                    ("font-weight", "700"),
                    ("font-style", "italic"),
                ]
            ),
            "Sans": Style(
                [
                    ("font-family", "Sans"),
                    ("font-stretch", "normal"),
                    ("font-weight", "400"),
                    ("font-style", "normal"),
                ]
            ),
            "Sans Bold": Style(
                [
                    ("font-family", "Sans"),
                    ("font-stretch", "normal"),
                    ("font-weight", "700"),
                    ("font-style", "normal"),
                ]
            ),
            "Sans Italic": Style(
                [
                    ("font-family", "Sans"),
                    ("font-stretch", "normal"),
                    ("font-weight", "400"),
                    ("font-style", "italic"),
                ]
            ),
            "Sans Bold Italic": Style(
                [
                    ("font-family", "Sans"),
                    ("font-stretch", "normal"),
                    ("font-weight", "700"),
                    ("font-style", "italic"),
                ]
            ),
            "Serif": Style(
                [
                    ("font-family", "Serif"),
                    ("font-stretch", "normal"),
                    ("font-weight", "400"),
                    ("font-style", "normal"),
                ]
            ),
            "Serif Bold": Style(
                [
                    ("font-family", "Serif"),
                    ("font-stretch", "normal"),
                    ("font-weight", "700"),
                    ("font-style", "normal"),
                ]
            ),
            "Serif Italic": Style(
                [
                    ("font-family", "Serif"),
                    ("font-stretch", "normal"),
                    ("font-weight", "400"),
                    ("font-style", "italic"),
                ]
            ),
            "Serif Bold Italic": Style(
                [
                    ("font-family", "Serif"),
                    ("font-stretch", "normal"),
                    ("font-weight", "700"),
                    ("font-style", "italic"),
                ]
            ),
            "System-ui": Style(
                [
                    ("font-family", "System-ui"),
                    ("font-stretch", "normal"),
                    ("font-weight", "400"),
                    ("font-style", "normal"),
                ]
            ),
            "System-ui Bold": Style(
                [
                    ("font-family", "System-ui"),
                    ("font-stretch", "normal"),
                    ("font-weight", "700"),
                    ("font-style", "normal"),
                ]
            ),
            "System-ui Italic": Style(
                [
                    ("font-family", "System-ui"),
                    ("font-stretch", "normal"),
                    ("font-weight", "400"),
                    ("font-style", "italic"),
                ]
            ),
            "System-ui Bold Italic": Style(
                [
                    ("font-family", "System-ui"),
                    ("font-stretch", "normal"),
                    ("font-weight", "700"),
                    ("font-style", "italic"),
                ]
            ),
        },
    }
    if len(sys.argv) > 1 and sys.argv[1] == "modify_svg":
        modify_svg(sys.argv[2])
