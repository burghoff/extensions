#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) 2021 David Burghoff
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

import inkex

import os

pytest_current_test = os.getenv("PYTEST_CURRENT_TEST")
if (
    not pytest_current_test
    == "tests/test_inkex_inx.py::InxTestCase::test_inx_files (call)"
):
    import inkex.text


class TextHighlight(inkex.EffectExtension):
    def add_arguments(self, pars):
        pars.add_argument("--tab", help="The selected UI-tab when OK was pressed")

        pars.add_argument("--highlighttype", type=int, default=1, help="Highlight type")

    def effect(self):
        sel = [self.svg.selection[ii] for ii in range(len(self.svg.selection))]
        # should work with both v1.0 and v1.1
        seld = [v for el in sel for v in el.descendants2()]

        if self.options.highlighttype == 1:
            htype = "char"
        elif self.options.highlighttype == 2:
            htype = "charink"
        elif self.options.highlighttype == 3:
            htype = "chunk"
        elif self.options.highlighttype == 4:
            htype = "chunkink"
        elif self.options.highlighttype == 5:
            htype = "line"
        elif self.options.highlighttype == 6:
            htype = "lineink"
        elif self.options.highlighttype == 7:
            htype = "full"
        elif self.options.highlighttype == 8:
            htype = "fullink"

        for el in seld:
            if isinstance(el, (inkex.TextElement, inkex.FlowRoot)):
                el.parsed_text.Make_Highlights(htype)


if __name__ == "__main__":
    TextHighlight().run()
